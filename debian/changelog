chron (2.3-62-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 31 Dec 2024 08:29:18 -0600

chron (2.3-61-2) unstable; urgency=medium

  * Rebuilding for unstable following bookworm release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 18 Jun 2023 11:20:18 -0500

chron (2.3-61-1) experimental; urgency=medium

  * New upstream release (into 'experimental' while Debian is frozen)

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 08 May 2023 17:20:09 -0500

chron (2.3-60-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 07 Mar 2023 19:25:25 -0600

chron (2.3-59-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 31 Jan 2023 12:31:21 -0600

chron (2.3-58-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 04 Oct 2022 14:03:55 -0500

chron (2.3-57-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 03 Jun 2022 07:19:46 -0500

chron (2.3-56-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 18 Aug 2020 07:52:58 -0500

chron (2.3-55-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 04 Feb 2020 09:26:03 -0600

chron (2.3-54-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 27 Aug 2019 09:04:11 -0500

chron (2.3-53-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 12 Sep 2018 19:26:15 -0500

chron (2.3-52-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 06 Jan 2018 13:07:23 -0600

chron (2.3-51-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/compat: Increase level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Oct 2017 11:46:30 -0500

chron (2.3-50-2) unstable; urgency=medium

  * Rebuilt under R 3.4.0 to update registration for .C() and .Fortran()
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 03 Jun 2017 17:39:32 -0500

chron (2.3-50-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Add ${misc:Depends} to Depends

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 20 Feb 2017 21:01:00 -0600

chron (2.3-49-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 19 Jan 2017 21:23:17 -0600

chron (2.3-48-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 09 Dec 2016 11:40:05 -0600

chron (2.3-47-2) unstable; urgency=medium

  * debian/compat: Created			(Closes: #829353)
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 07 Jul 2016 05:46:38 -0500

chron (2.3-47-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 27 Jun 2015 07:05:29 -0500

chron (2.3-46-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 19 Jun 2015 09:11:17 -0500

chron (2.3-45-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 11 Feb 2014 19:56:33 -0600

chron (2.3-44-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Sep 2013 08:35:51 -0500

chron (2.3-43-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 30 Mar 2013 08:30:59 -0500

chron (2.3-43-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 14 Nov 2012 08:56:38 -0600

chron (2.3-42-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 22 Aug 2011 10:16:11 -0500

chron (2.3-41-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 08 Aug 2011 20:23:15 -0500

chron (2.3-40-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 24 Jul 2011 16:45:52 -0500

chron (2.3-39-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 07 Dec 2010 15:30:36 -0600

chron (2.3-38-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 08 Oct 2010 09:01:44 -0500

chron (2.3-37-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 15 Sep 2010 14:33:46 -0500

chron (2.3-36-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 02 Sep 2010 19:13:58 -0500

chron (2.3-35-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 05 Apr 2010 21:21:52 -0500

chron (2.3-34-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 17 Mar 2010 12:47:11 -0500

chron (2.3-33-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 08 Nov 2009 20:37:37 -0600

chron (2.3-32-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Nov 2009 09:40:21 -0600

chron (2.3-32-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 04 Oct 2009 19:17:02 -0500
  
chron (2.3-31-1) unstable; urgency=low

  * New upstream release

  * debian/control: Changed Section: to 'gnu-r'

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 10 Sep 2009 16:01:57 -0500

chron (2.3-30-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 18 Feb 2009 20:13:52 -0600

chron (2.3-29-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 05 Feb 2009 21:25:10 -0600

chron (2.3-28-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 05 Jan 2009 22:29:18 -0600

chron (2.3-27-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 17 Dec 2008 07:59:22 -0600

chron (2.3-26-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 14 Dec 2008 10:26:23 -0600

chron (2.3-25-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 15 Nov 2008 15:29:28 -0600

chron (2.3-24-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 18 Jul 2008 08:08:57 -0500

chron (2.3-23-1) unstable; urgency=low

  * New upstream release
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 20 May 2008 18:12:49 -0500

chron (2.3-22-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 05 Mar 2008 07:12:52 -0600

chron (2.3-21-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 14 Feb 2008 18:38:03 -0600

chron (2.3-20-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 07 Feb 2008 07:31:29 -0600

chron (2.3-19-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 05 Feb 2008 21:31:50 -0600

chron (2.3-18-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 31 Jan 2008 14:20:38 -0600

chron (2.3-17-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 30 Jan 2008 18:23:19 -0600

chron (2.3-16-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.7.3

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 25 Dec 2007 10:09:39 -0600

chron (2.3-15-1) unstable; urgency=low

  * New upstream release
  
  * Built with R 2.6.0 release candidate, setting (Build-)Depends: to 
    'r-base-(core|dev) >> 2.5.1' to prevent move to testing before R 2.6.0

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 17 Sep 2007 09:24:33 -0500

chron (2.3-14-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 13 Jul 2007 14:09:23 -0500

chron (2.3-13-1) unstable; urgency=low

  * New upstream release
  * debian/control: Updated (Build-)Depends: to 'r-base-(core|dev) (>= 2.5.1)'

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 10 Jul 2007 23:01:12 -0500

chron (2.3-12-1) unstable; urgency=low

  * New upstream release
  * debian/control: (Build-)Depends: updated to r-base-dev (>= 2.5.1~20070618)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 18 Jun 2007 19:56:30 -0500

chron (2.3-11-1) unstable; urgency=low

  * New upstream release

  * debian/control: Updated (Build-)Depends: to 'r-base-core (>= 2.5.0)'
  
 -- Dirk Eddelbuettel <edd@debian.org>  Thu,  3 May 2007 14:02:18 -0500

chron (2.3-10-1) unstable; urgency=low

  * New upstream release
  * debian/control: Updated (Build-)Depends: to 'r-base-core (>= 2.4.1)'

 -- Dirk Eddelbuettel <edd@debian.org>  Thu,  1 Feb 2007 10:12:46 -0600

chron (2.3-9-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Oct 2006 21:05:49 -0500

chron (2.3-8-1) unstable; urgency=low

  * Initial Debian release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  7 Oct 2006 09:12:40 -0500


